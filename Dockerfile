FROM continuumio/miniconda3:24.5.0-0

ENV HOME=/home/jovyan
ENV PYTHON_VERSION=3.9.19
ENV ENV_FILE=qiime2-2024.5-py39-linux-conda.yml

ENV QUAST_REPO=https://github.com/misialq/quast.git
ENV Q2_TYPES_REPO=https://github.com/qiime2/q2-types.git
ENV Q2_MOSHPIT_REPO=https://github.com/misialq/q2-moshpit.git
ENV Q2_ASSEMBLY_REPO=https://github.com/bokulich-lab/q2-assembly.git

ENV QUAST_BRANCH=issue-230
ENV Q2_TYPES_BRANCH=a22a64a
ENV Q2_MOSHPIT_BRANCH=annot-improv-with-norm
ENV Q2_ASSEMBLY_BRANCH=00f945a

USER root

# Set the image shell to bash
SHELL ["/bin/bash", "-c"]

# Point sh to bash rather than dash
RUN ln -sf /bin/bash /bin/sh

# Add jovyan user for the hub
RUN useradd -m jovyan && chown -R jovyan:jovyan $HOME

# Install ETHZ root ca certificates
ADD http://pkiaia.ethz.ch/aia/ETHZRootCA2020.pem /usr/local/share/ca-certificates/ETHZRootCA2020.crt
ADD http://pkiaia.ethz.ch/aia/ETHZIssuingCA2020.pem /usr/local/share/ca-certificates/ETHZIssuingCA2020.crt
ADD http://pkiaia.ethz.ch/aia/DigiCertGlobalRootCA.pem /usr/local/share/ca-certificates/DigiCertGlobalRootCA.crt
ADD http://pkiaia.ethz.ch/aia/DigiCertTLSRSASHA2562020CA1-1.pem /usr/local/share/ca-certificates/DigiCertTLSRSASHA2562020CA1-1.crt
ADD http://pkiaia.ethz.ch/aia/DigiCertGlobalRootG2.pem /usr/local/share/ca-certificates/DigiCertGlobalRootG2.crt
ADD http://pkiaia.ethz.ch/aia/DigiCertGlobalG2TLSRSASHA2562020CA1.pem /usr/local/share/ca-certificates/DigiCertGlobalG2TLSRSASHA2562020CA1.crt
RUN chmod 644 /usr/local/share/ca-certificates/* && update-ca-certificates

# OS update, basic addon commands, fix the timezone
RUN apt-get update && apt-get upgrade -y && \
    apt-get install -y python3-pip texlive-xetex lmodern zip less vim man gpg && \
    apt-get clean && \
    chown -R 1000 /opt/conda && \
    mkdir /data && \
    ln -sf /usr/share/zoneinfo/Europe/Zurich /etc/localtime && echo "Europe/Zurich" >> /etc/timezone

# Export some conda variables
RUN echo '#!/bin/bash' > /etc/profile.d/conda_fix.sh && \
    echo 'export CONDA_ENVS_PATH=$HOME/.conda/envs CONDA_PREFIX=/opt/conda CONDA_AUTO_ACTIVATE_BASE=true' >> /etc/profile.d/conda_fix.sh && \
    echo 'conda init bash > /dev/null 2>&1' >> /etc/profile.d/conda_fix.sh && \
    echo 'if [ -f $HOME/.bashrc ]; then source $HOME/.bashrc; fi' >> /etc/profile.d/conda_fix.sh

# Switch the user - if we set the user at the very end rather than here, we need to chown again all the files
# created by the commands below
USER 1000
# Jupyter stuff
RUN conda install -y -c conda-forge "jupyterhub>=3.0.0" mamba python=$PYTHON_VERSION

# Install QIIME 2 with all its dependencies + some JLab goodies
WORKDIR $HOME
COPY $ENV_FILE .
RUN mamba env update -n base --file $ENV_FILE
RUN mamba install -y -n base \
      -c conda-forge -c bioconda -c https://packages.qiime2.org/qiime2/2024.5/metagenome/staged -c picrust \
      q2-assembly q2-moshpit q2-fondue q2templates tqdm xmltodict pyhmmer sourmash shortuuid frictionless \
      q2-picrust2 ncbi-datasets-pylib xmltodict gh \
      jupyterlab-system-monitor jupyterlab-git \
      jupyter_contrib_nbextensions webcolors uri-template isoduration fqdn \
      uncertainties nbgrader pylatex biopython ete3 nbgitpuller && \
    pip install git+https://github.com/bokulich-lab/RESCRIPt.git@f872c4b git+https://github.com/dib-lab/q2-sourmash \
      git+https://github.com/bokulich-lab/q2-kmerizer git+https://github.com/qiime2/q2-vizard.git git+https://github.com/qiime2/q2-stats.git \
      cython && \
    rm $ENV_FILE && \
    jupyter serverextension enable --py qiime2 --sys-prefix && \
    jupyter labextension disable "@jupyterlab/apputils-extension:announcements" && \
    conda clean --all -y

# Temporarily install the patched version of QUAST and update the shotgun Q2 plugins
# for whatever reason, this does not work with pip directly - need to clone first
WORKDIR /tmp

RUN git clone $QUAST_REPO quast && cd quast && git checkout $QUAST_BRANCH && \
    mamba run -n base pip install . && cd .. && rm -rf /tmp/quast && \
    git clone $Q2_TYPES_REPO q2-types && cd q2-types && git checkout $Q2_TYPES_BRANCH && \
    mamba run -n base pip install . && cd .. && rm -rf /tmp/q2-types && \
    git clone $Q2_MOSHPIT_REPO q2-moshpit && cd q2-moshpit && git checkout $Q2_MOSHPIT_BRANCH && \
    mamba run -n base pip install . && cd .. && rm -rf /tmp/q2-moshpit && \
    git clone $Q2_ASSEMBLY_REPO q2-assembly && cd q2-assembly && git checkout $Q2_ASSEMBLY_BRANCH && \
    mamba run -n base pip install . && cd .. && rm -rf /tmp/q2-assembly && \
    mamba run -n base pip cache purge

WORKDIR $HOME
RUN qiime dev refresh-cache

# Add the CONDA_PREFIX variable to the kernel spec for QIIME2 to work properly
# Change the name of the default kernel
RUN jq '.env.CONDA_PREFIX="/opt/conda"' /opt/conda/share/jupyter/kernels/python3/kernel.json > temp.json && \
    mv temp.json /opt/conda/share/jupyter/kernels/python3/kernel.json && \
    jq '.display_name="QIIME 2"' /opt/conda/share/jupyter/kernels/python3/kernel.json > temp.json && \
    mv temp.json /opt/conda/share/jupyter/kernels/python3/kernel.json

CMD ["jupyterhub-singleuser"]
